terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
    }
  }
}

provider "aws" {
  region     = "us-east-1"
  access_key = "INSERT ACCESS KEY HERE"
  secret_key = "INSERT SECRET KEY HERE"
}

