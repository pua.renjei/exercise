resource "aws_iam_user" "demo" {
  name = "demo-user"
}

resource "aws_iam_policy_attachment" "demo" {
  name       = "demo"
  users      = [aws_iam_user.demo.name]
  policy_arn = "arn:aws:iam::aws:policy/ReadOnlyAccess"
}

resource "aws_iam_user_login_profile" "demo" {
  user    = aws_iam_user.demo.name
}

resource "aws_iam_access_key" "demo" {
  user = aws_iam_user.demo.name
}

output "account_id" {
  value = aws_iam_user.demo.unique_id
}

output "username" {
  value = aws_iam_user.demo.name
}

output "user_password" {
  value = aws_iam_user_login_profile.demo.password
}

output "access_key" {
  value = aws_iam_access_key.demo.secret
  sensitive = true
}

output "access_key_id" {
  value = aws_iam_access_key.demo.id
}
