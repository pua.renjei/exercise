## Create VPC
resource "aws_vpc" "demo" {
  cidr_block       = "172.10.0.0/16"
  instance_tenancy = "default"
  enable_dns_hostnames = true
  
  tags = {
    Name = "demo"
  }
}

resource "aws_internet_gateway" "demo" {
  vpc_id = aws_vpc.demo.id

  tags = {
    Name = "demo"
  }
}

resource "aws_subnet" "demo_public" {
  vpc_id     = aws_vpc.demo.id
  cidr_block = "172.10.1.0/24"
  enable_resource_name_dns_a_record_on_launch = true
  availability_zone = "us-east-1a"
  tags = {
    Name = "demo_public_subnet"
  }
}

resource "aws_subnet" "demo_private" {
  vpc_id     = aws_vpc.demo.id
  cidr_block = "172.10.2.0/24"
  enable_resource_name_dns_a_record_on_launch = true
  availability_zone = "us-east-1a"
  tags = {
    Name = "demo_private_subnet"
  }
}

resource "aws_route_table" "demo_public" {
  vpc_id = aws_vpc.demo.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.demo.id
  }

  tags = {
    Name = "demo_public_route"
  }
}

resource "aws_route_table" "demo_private" {
  vpc_id = aws_vpc.demo.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_nat_gateway.nat_gw.id
  }

  tags = {
    Name = "demo_private_route"
  }
}

resource "aws_route_table_association" "demo_public" {
  subnet_id      = aws_subnet.demo_public.id
  route_table_id = aws_route_table.demo_public.id
}

resource "aws_route_table_association" "demo_private" {
  subnet_id      = aws_subnet.demo_private.id
  route_table_id = aws_route_table.demo_private.id
}


resource "aws_eip" "nat_gw" {
  domain   = "vpc"
}

resource "aws_nat_gateway" "nat_gw" {
  allocation_id = aws_eip.nat_gw.id
  subnet_id     = aws_subnet.demo_public.id
  depends_on = [aws_internet_gateway.demo]
}