# Instructions

Use `terraform init` to initialize a new working directory.

Use `terraform plan` to preview deployment.

Use `terraform apply` and enter `yes` on prompt to start deployment.

You will need to fill in the access key and secret key in provider.tf.