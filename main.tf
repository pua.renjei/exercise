resource "aws_instance" "web" {
  ami = "ami-053b0d53c279acc90"
  instance_type = "t2.micro"
  associate_public_ip_address = false
  subnet_id = aws_subnet.demo_private.id
  security_groups = [aws_security_group.demo.id]
  key_name = "demo"
  
  tags = {
    Name = "web"
  }

  user_data  = <<-EOF
                #!/bin/bash
                sudo apt-get update
                sudo apt install docker.io -y
                sudo usermod -a -G docker ubuntu
                mkdir /home/ubuntu/web
                docker run --name web -p 80:80 -d nginx:1.18.0\
                EOF

  depends_on = [aws_nat_gateway.nat_gw]         
}

resource "null_resource" "copy-job" {

  connection {
    type = "ssh"
    user = "ubuntu"
    private_key = "${file("key/demo.pem")}"
    host = "${aws_lb.demo_lb.dns_name}"
    port = 22
  }

  provisioner "file" {
    source = "index.html"
    destination = "/home/ubuntu/index.html"
  }

  provisioner "remote-exec" {
    inline = [
      "sudo docker cp /home/ubuntu/index.html web:/usr/share/nginx/html/index.html"
    ]
  }

  depends_on = [aws_instance.web, aws_lb.demo_lb] 
}



resource "aws_key_pair" "demo" {
  key_name   = "demo"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCh2tjoFyp+HJI2R8aYaXzCJ8EkwFsvse6yEzmUFDEZ8iEIm/wRKzaCV/vAA0hD8AGIrPjJ+PZnM2/KB6hYQWEYIFxZivCqG5h7JMOIBWmyFkflktlnnseOLb5yh4RSo8r+DQ/4f4ALpNXPryoTwUnByXaUxjkTVrFqmp0KRi96ROevU0tLkhHF0R5BLFzbBbE+OH0Xe33lGrxvTgiywwcy14Ki6H+wGMgTNimsdEyyQ5Ipg/P7q5+vL2Rz8xWJnwySUmUjb4ENR0Vd5vpBtSYYf+qjB0BX0W43weZmVfFxjESkx2s/fsXycUMedoQ4kuBQ/3CTUEDXTY8jEgLeWRNv"
}

resource "aws_lb" "demo_lb" {
  name               = "demo-lb"
  internal           = false
  load_balancer_type = "network"
  subnets            = [aws_subnet.demo_public.id]
}


resource "aws_lb_listener" "demo_http" {
  load_balancer_arn = aws_lb.demo_lb.arn
  port              = "80"
  protocol          = "TCP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.demo_http.arn
  }
}

resource "aws_lb_target_group" "demo_http" {
  name        = "Web-HTTP"
  target_type = "instance"
  port        = 80
  protocol    = "TCP"
  vpc_id      = aws_vpc.demo.id
}


resource "aws_lb_target_group_attachment" "demo_http" {
  target_group_arn = aws_lb_target_group.demo_http.arn
  target_id        = aws_instance.web.id
  port             = 80
}

resource "aws_lb_listener" "demo_ssh" {
  load_balancer_arn = aws_lb.demo_lb.arn
  port              = "22"
  protocol          = "TCP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.demo_ssh.arn
  }
}

resource "aws_lb_target_group" "demo_ssh" {
  name        = "Web-SSH"
  target_type = "instance"
  port        = 22
  protocol    = "TCP"
  vpc_id      = aws_vpc.demo.id
}


resource "aws_lb_target_group_attachment" "demo_ssh" {
  target_group_arn = aws_lb_target_group.demo_ssh.arn
  target_id        = aws_instance.web.id
  port             = 22
}

